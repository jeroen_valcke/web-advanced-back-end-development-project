var init = function (io){
    var socketioJwt = require("socketio-jwt");
    var jwtSecret = require('../config/jwtSecret');

    console.log("init");

    io.set("authorization", socketioJwt.authorize({
        secret: jwtSecret,
        handshake: true
    }));

    io.sockets.on("connection", function (socket) {

        var leaveAllSockets = function(){
            for(name in socket.rooms){
                socket.leave(socket.rooms[name]);
            }
        };
        socket.leave(socket.id);
        socket.join("main");

        socket.on("getRooms", function(){
            socket.emit("roomlist", io.sockets.adapter.rooms);
        });

        socket.on("selectRoom", function(data){
            if(data.room){
                leaveAllSockets();
                socket.join(data.room);
            }
        });

        socket.on("send", function (data) {
            console.log(data);
            var username = data.username;
            var message = data.message;
            var datetime = data.datetime;

            socket.broadcast.to(socket.rooms[0]).emit("send", data);
        });
        socket.on("busy", function () {
            socket.emit("busy");
            socket.broadcast.to(socket.rooms[0]).emit("busy");
        });
        socket.on("free", function () {
            socket.emit("free");
            socket.broadcast.to(socket.rooms[0]).emit("free");
        });
    });
}

module.exports = init;
