﻿var gulp = require("gulp"),
    uglify = require('gulp-uglify'),
    concat = require('gulp-concat'),
    jshint = require('gulp-jshint');

gulp.task("minify", function () {
    return gulp.src(['public/app/**/*.js', '!public/app/**/*min.js'])
       .pipe(jshint())
       .pipe(jshint.reporter('default'))
       .pipe(uglify())
       .pipe(concat('app.js'))
       .pipe(gulp.dest('public/scripts/'));
});