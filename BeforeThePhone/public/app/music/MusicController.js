﻿var MusicController = function ($scope) {
    $scope.selectedMusic = null;
    $scope.Music = [];
    
    var newSongSound = new Music("Changing tape...", "", 0, "/files/sounds/new_song.mp3");
    var player;
    var reversLoop;
    var run;
    var lockcontrols = false;
    
    var init = function () {
        $scope.Music =
            [
            new Music("Riders on The storm", "The Doors", 1972, "/files/music/The Doors - Riders On the Storm (Remastered HD).mp3"),
            new Music("Black Coffee", "Peggy Lee", 1953, "/files/music/Peggy Lee - Black Coffee.mp3"),
            new Music("What a wonderful world", "Louis Armstrong", 1967, "/files/music/Louis Armstrong What A Wonderful World.mp3")
        ];
        
        player = document.getElementById("player");
        $scope.selectedMusic = $scope.Music[0];
        //player.play();
        console.log(player);
    };
    
    init();
    
    $scope.play = function () {
        if ((player.paused || player.playbackRate != 1) && !lockcontrols) {
            $(".player_button").css("background-image", "url('')");
            $("#player_play").css("background-image", "url('/img/music_button_pressed.png')");
            $(".rotater").attr("src", "/img/rotate_play.GIF");
            
            stopReverse();
            console.log(player);
            player.playbackRate = 1;
            player.play();
            player.playbackRate = 1;
        }
    };
    
    $scope.pauze = function () {
        if (!player.paused && !lockcontrols) {
            $(".player_button").css("background-image", "none");
            $("#player_pause").css("background-image", "url('/img/music_button_pressed.png')");
            $(".rotater").attr("src", "/img/rotate_static.png");
            
            stopReverse();
            player.pause();
        }
    };
    
    $scope.forward = function () {
        if (!lockcontrols) {
            $(".player_button").css("background-image", "none");
            $("#player_forward").css("background-image", "url('/img/music_button_pressed.png')");
            $(".rotater").attr("src", "/img/rotate_fast_forward.GIF");
            
            stopReverse();
            player.playbackRate = 6;
            player.play();
            player.playbackRate = 6;
        }
    };
    
    $scope.reverse = function () {
        if (!lockcontrols) {
            $(".player_button").css("background-image", "none");
            $("#player_reverse").css("background-image", "url('/img/music_button_pressed.png')");
            $(".rotater").attr("src", "/img/rotate_reverse.GIF");
            
            player.play();
            reversePlayer(player);
            
            $("#div").animate({ "width": 50 }, 0).delay(500).animate({ "width": 100 }).delay(500).animate({ "width": 150 });
        }
    };
    
    $scope.stop = function () {
        if (!lockcontrols) {
            $(".player_button").css("background-image", "none");
            $("#player_stop").css("background-image", "url('/img/music_button_pressed.png')");
            $(".rotater").attr("src", "/img/rotate_static.png");
            
            stopReverse();
            player.pause();
            player.currentTime = 0;
        }
    };
    
    $scope.changeSong = function (m) {
        stopReverse();
        if (!player.paused)
            player.pause();
        
        $scope.selectedMusic = newSongSound;
        $scope.$apply();
        
        console.log($scope.selectedMusic);
        
        lockcontrols = true;
        player.load();
        player.play();
        
        player.addEventListener("ended", function () {
            $scope.selectedMusic = m;
            $scope.$apply();
            
            console.log($scope.selectedMusic);
            
            player.load();
            lockcontrols = false;
        });
        
        $(".player_button").css("background-image", "none");
        $(".rotater").attr("src", "/img/rotate_static.png");
    };
    
    function reversePlayer(p) {
        p.pause();
        run = true;
        reverseTick();
    }
    
    function reverseTick() {
        setTimeout(function () {
            var time = player.currentTime;
            if (time > 1)
                player.currentTime = (time - 1);
            
            if (run)
                reverseTick();
        }, 166);
    }
    
    function stopReverse() {
        run = false;
    }
};

MusicController.$inject = ["$scope"];