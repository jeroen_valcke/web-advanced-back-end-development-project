﻿function Music(name, artist, year, resource) {
    this.name = name;
    this.artist = artist;
    this.year = year;
    this.resource = resource;
}

Music.prototype = {
    get Name() { return this.name; },
    set Name(v) { this.name = v; },
    
    get Artist() { return this.artist; },
    set Artist(v) { this.artist = v; },
    
    get Year() { return this.year; },
    set Year(v) { this.year = v; },
    
    get Resource() { return this.resource; },
    set Resource(v) { this.resource = v; },
    
    get Img() { return "/img/cassette-image.png"; },
};