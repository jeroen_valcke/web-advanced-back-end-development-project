﻿var NoteController = function ($scope, $sce) {
    $scope.pages = [];
    $scope.selectedPage = null;
    $scope.pageIndex = 0;
    var canvas_id;
    var canvas;
    var ctx;
    var startx,
        starty;
    var drawing;
    var noteEl;
    var lineLimit;
    var lineHeight;
    var noteElHeight;
    
    var init = function () {
        canvas_id = "noteCanvas";
        canvas = document.getElementById(canvas_id);
        ctx = canvas.getContext("2d");
        drawing = false;
        noteEl = $("#note");
        lineLimit = 11;
        lineHeight = parseInt(noteEl.css('line-height'));
        noteEl.autosize();
        
        $scope.selectedPage = new Page("test", $sce.trustAsHtml(""), Date.now());
        $scope.selectedPage.Canvas = canvas;
        $scope.pages.push($scope.selectedPage);
        
        $("#Page_back").click(back);
        $("#Page_del").click(del);
        $("#Page_forward").click(forward);
    };
    
    var back = function () {
        //console.log($scope.pageIndex);
        if ($scope.pageIndex !== 0)
            if ($scope.pages[$scope.pageIndex - 1]) {
                //console.log("back");
                
                $scope.selectedPage.Canvas = canvas;
                
                $scope.pageIndex -= 1;
                $scope.selectedPage = $scope.pages[$scope.pageIndex];
                
                setCanvas(false);
            }
        
        $scope.$apply();
    };
    
    var del = function () {
        $scope.pages.splice($scope.pageIndex, 1);
        //console.log($scope.pages);
        if ($scope.pageIndex !== 0) {
            $scope.pageIndex -= 1;
            $scope.selectedPage = $scope.pages[$scope.pageIndex];
            setCanvas(false);
        } else {
            if ($scope.pages.length === 0) {
                $scope.selectedPage = new Page("test", $sce.trustAsHtml(""), Date.now());
                $scope.pages.push($scope.selectedPage);
                setCanvas(true);
            }else{
                $scope.selectedPage = $scope.pages[$scope.pageIndex];
                setCanvas(false);
            }
        }

        $scope.$apply();
    };
    
    var forward = function () {
        //console.log($scope.pageIndex);
        //console.log($scope.pages.length - 1);
        if ($scope.pageIndex < $scope.pages.length - 1) {
            $scope.selectedPage.Canvas = canvas;
            $scope.pageIndex += 1;
            $scope.selectedPage = $scope.pages[$scope.pageIndex];
            
            setCanvas(false);
        } else {
            //console.log("forward");
            $scope.selectedPage.Canvas = canvas;
            $scope.selectedPage = new Page("test", $sce.trustAsHtml(""), Date.now());
            $scope.pages.push($scope.selectedPage);
            $scope.pageIndex += 1;
            
            setCanvas(true);
        }
        
        $scope.$apply();
        //console.log($scope.pages);
    };
    
    var setCanvas = function (bool) {
        //console.log(bool);
        if (bool) {
            canvas = document.createElement("CANVAS");
            canvas.setAttribute("id", "noteCanvas");
            canvas.setAttribute("width", "185");
            canvas.setAttribute("height", "255");
        }
        else
            canvas = $scope.selectedPage.Canvas;
        
        ctx = canvas.getContext("2d");
        $("#noteCanvasContainer").html("");
        $("#noteCanvasContainer").append(canvas);
        
        $(canvas).mousedown(function (e) { handlePress(e); });
        $(canvas).mousemove(function (e) { handleMove(e); });
        $(canvas).mouseup(function (e) { handleRelease(e); });
    };
    
    init();
    
    $(canvas).mousedown(function (e) { handlePress(e); });
    $(canvas).mousemove(function (e) { handleMove(e); });
    $(canvas).mouseup(function (e) { handleRelease(e); });
    
    var handlePress = function (e) {
        //console.log("pressed");
        startx = e.pageX - this.offsetLeft;
        starty = e.pageY - this.offsetTop;
        drawing = true;
    };
    
    var handleMove = function (e) {
        if (drawing) {
            //console.log("drawing");
            var newX = e.pageX - noteEl.offset().left,
                newY = e.pageY - noteEl.offset().top;
            
            ctx.beginPath();
            ctx.moveTo(startx, starty);
            ctx.lineTo(newX, newY);
            ctx.strokeStyle = "black";
            ctx.lineWidth = 1;
            ctx.stroke();
            
            startx = newX;
            starty = newY;
        }
    };
    
    var handleRelease = function (e) {
        //console.log("release");
        drawing = false;
        noteEl.focus();
    }
    ;
    $(document).keydown(function (e) {
        var nodeName = e.target.nodeName.toLowerCase();
        //console.log(e.which);
        if (nodeName === 'textarea') {
            if (e.which === 8 || (e.which <= 40 && e.which >= 33) || e.which == 9) {
                return false;
            }
            noteElHeight = noteEl.height();
            var linesUsed = noteElHeight / lineHeight;
            
            if (linesUsed > lineLimit + 1)
                return false;
        }
    });
};

NoteController.$inject = ["$scope", "$sce"];