﻿function Page(title, text, datetime, virtualText){
    this.title = title;
    this.text = text;
    this.datetime = datetime;
    if (virtualText)
        this.virtualText = virtualText;
    else
        this.virtualText = [];
}

Page.prototype = {
    get Title() { return this.title; },
    set Title(v) { this.title = v; },
    
    get Text() { return this.text; },
    set Text(v) { this.text = v; },
    
    get Datetime() { return this.datetime; },
    set Datetime(v) { this.datetime = v; },
    
    get VirtualText() { return this.virtualText; },
    set VirtualText(v) { this.virtualText = v; },
    
    get Canvas() { return this.canvas; },
    set Canvas(v) { this.canvas = v; },
};