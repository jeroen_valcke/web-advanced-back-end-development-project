﻿var HomeController = function ($scope) {
    $scope.app = "";
    
    $scope.start = function (app) {
        if ($scope.app != "/app/" + app + "/" + app + ".html") {
            $scope.app = "/app/" + app + "/" + app + ".html";
            $scope.changeDetail(app);
        }
        
        $("#homescreen_" + app).animate({ opacity: 0.5 }, 100, function () {
            $("#homescreen_" + app).animate({ opacity: 0 }, 100, function () {
                $("#app").css("visibility", "visible");
                $("#app").css("display", "block");
                $("#homescreen").css("background-image", "url('/img/app_template.jpg')");
            });
        });
    };
    
    $scope.home = function () {
        $("#app").css({ "display": "none" });
        $("#app").css({ "visibility": "hidden" });
        $scope.app = "";
        $scope.changeDetail("home");
        $("#homescreen").css("background-image", "url('/img/homescreen.jpg')");
        $(".zoomContainer").remove();
    };
    
    initTime();
    
    function initTime() {
        var divTime = document.getElementById("time");
        var timeInterval = setInterval(function () {
            var currentTime = new Date();
            var currentHours = currentTime.getHours();
            var currentMinutes = currentTime.getMinutes();
            currentMinutes = (currentMinutes < 10 ? "0" : "") + currentMinutes;
            
            divTime.innerHTML = currentHours + ":" + currentMinutes;
        }, 1500);
    }
};

HomeController.$inject = ["$scope"];