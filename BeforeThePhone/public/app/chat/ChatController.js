﻿var ChatController = function ($scope, $sce) {
    $scope.messageBoard = "";
    $scope.messageText = "";
    $scope.messageMorse = "";
    
    $scope.txtUsername = "";
    $scope.txtPassword = "";
    
    $scope.txtSPassword = "";
    $scope.txtSConfirm = "";
    
    $scope.errorMessage = "";

    $scope.roomList = [];
    $scope.newRoomName = "";

    $scope.showRooms= false;
    $scope.showControls = false;
    
    var token;
    
    var morseWordPosition = 0;
    var morseLetterPositon = 0;
    
    var socket;
    
    var wordArr;
    var arrSentence;
    
    var shortBeep = document.getElementById("shortBeep");
    var longBeep = document.getElementById("longBeep");
    
    $scope.playMorse = function (morse) {
        morseWordPosition = 0;
        morseLetterPositon = 0;
        morse.trim();
        playSentence(morse);
    };
    
    function playSentence(sentence) {
        arrSentence = sentence.split(" ");
        playWord(arrSentence[morseWordPosition], sentenceCallback);
    }
    
    function playWord(word, callback) {
        wordArr = word.split("");
        //console.log(wordArr);
        playLetter(wordArr[morseLetterPositon], wordCallback);
    }
    
    function playLetter(letter, callback) {
        switch (letter) {
            case ".":
                //console.log(".")
                shortBeep.play();
                shortBeep.addEventListener('ended', callback);
                break;
            case "-":
                //console.log("-");
                longBeep.play();
                longBeep.addEventListener('ended', callback);
                break;
            case "/":
                //console.log("/");
                setTimeout(callback, 150);
                break;
        }
    }
    
    var playCallback = function () {
        socket.emit("free");
    };
    
    var wordCallback = function () {
        morseLetterPositon++;
        if (morseLetterPositon < wordArr.length) {
            playLetter(wordArr[morseLetterPositon], wordCallback);
        } else {
            morseLetterPositon = 0;
            setTimeout(sentenceCallback, 150);
        }
    };
    
    var sentenceCallback = function () {
        morseWordPosition++;
        if (morseWordPosition < (arrSentence.length - 1)) {
            playWord(arrSentence[morseWordPosition], sentenceCallback);
        } else {
            morseWordPosition = 0;
            playCallback();
            return;
        }
    };
    
    $scope.noManual = function () {
        $("#messageManual").css("display", "none");
        $("#messageText").css("display", "block");
        $("#messageBoard").removeClass("messageBoard_manual").addClass("messageBoard_text");
        
        
        $(".messageTab").removeClass("messageTabSelected");
        $(".messageTab:last()").addClass("messageTabSelected");
    };
    
    $scope.manual = function () {
        $("#messageText").css("display", "none");
        $("#messageManual").css("display", "block");
        $("#messageBoard").removeClass("messageBoard_text").addClass("messageBoard_manual");
        
        $(".messageTab").removeClass("messageTabSelected");
        $(".messageTab:first()").addClass("messageTabSelected");
    };
    
    $scope.submit = function () {
        if (!$scope.messageText)
            return;
        
        var message = {
            username: $scope.txtUsername,
            message: $scope.messageText,
            datetime: Date.now
        };
        
        setHtml(message, "messageSelf");
        
        socket.emit("send", message);
    };
    
    $scope.long = function () {
        $scope.messageMorse += "-";
    };
    
    $scope.short = function () {
        $scope.messageMorse += ".";
    };
    
    $scope.space = function () {
        $scope.messageMorse += " / ";
        $scope.messageText = MorseTeken.morseToText($scope.messageMorse);
    };
    
    $scope.next = function () {
        $scope.messageMorse += " ";
        $scope.messageText = MorseTeken.morseToText($scope.messageMorse);
    };
    
    $scope.undoLast = function () {
        var temp = $scope.messageMorse;
        var lastChar = temp.slice(-1);

        if(lastChar == " ")
            temp =  temp.slice(0,-1);

        if ((i = temp.lastIndexOf(" ")) !== -1)
            temp = temp.slice(0, i);
        else {
            $scope.undoAll();
            return;
        }
        
        $scope.messageMorse = temp;
        $scope.messageText = MorseTeken.morseToText(temp);
    };
    $scope.undoAll = function () {
        $scope.messageMorse = "";
        $scope.messageText = "";
    };
    
    $scope.TextChange = function () {
        $scope.messageMorse = MorseTeken.textToMorse($scope.messageText);
    };
    
    $scope.openSignup = function () {
        $scope.errorMessage = "";
        $(".chatDiv").css("display", "none");
        $("#signup").css("display", "block");
    };
    
    $scope.signup = function () {
        if ($scope.txtSPassword !== $scope.txtSConfirm) {
            Error("Your passwords do not match.");
            return;
        }
        
        var json = {
            'username': $scope.txtUsername,
            'password': $scope.txtSPassword
        };
        
        $.ajax({
            url: "/auth/signup",
            type: "POST",
            data: json
        }).done(function (data, err) {
            token = data.token;
            sessionStorage.setItem("loginObject", JSON.stringify(data));
            connect_socket(token, data.username);
            $(".chatDiv").css("display", "none");
            $scope.showRooms = true;
        }).error(function (err) {
            if (err.status === 404) {
                Error("We were unable to reach te server. Please make sure you are online.");
            } else if (err.status === 400) {
                Error("Signup failed! Username already exists.");
            } else if (err.status >= 500 && err.status < 600) {
                Error("Oops! There seems to be a problem with our server. Please try again later.");
            } else {
                Error("Oops! An unknown error has occured. Please try again later.");
            }
        });
    };
    
    $scope.login = function () {
        var json = {
            'username': $scope.txtUsername,
            'password': $scope.txtPassword
        };
        
        $.ajax({
            url: "/auth/login",
            type: "POST",
            data: json
        }).done(function (data, err) {
            token = data.token;
            sessionStorage.setItem("loginObject", JSON.stringify(data));
            connect_socket(token, data.username);
            $(".chatDiv").css("display", "none");
            $scope.showRooms = true;
        }).error(function (err) {
            if (err.status === 400) {
                Error("Authentication failed! Pleae use a correct username and password.");
            } else if (err.status === 404 || err.status === 0) {
                Error("We were unable to reach te server. Please make sure you are online.");
            } else if (err.status >= 500 && err.status < 600) {
                Error("Oops! There seems to be a problem with our server. Please try again later.");
            } else {
                Error("Oops! An unknown error has occured. Please try again later.");
            }
        });
    };
    
    $scope.openLogin = function () {
        $scope.errorMessage = "";
        $(".chatDiv").css("display", "none");
        $("#login").css("display", "block");
    };

    $scope.backChat = function(){
        if($scope.showControls === true){
            $scope.showRooms = true;
            $scope.showControls = false;
        }else if($scope.showRooms === true){
            $scope.showRooms = false;
            $scope.showControls = false;
            $("#login").css("display", "block");
        }else{
            $scope.home();
        }
    };
    
    var connect_socket = function (tok, user) {
        /*socket = io("http://nophone-41691.onmodulus.net/", {
            query: 'token=' + tok
        });*/

        socket = io("http://localhost/", {
            query: 'token=' + tok
        });

        var getrooms = function(){
            socket.emit("getRooms");
        }

        window.setInterval(getrooms, 1000);

        socket.on("roomlist", function(data){
            $scope.roomList = [];
            for(el in data)
                $scope.roomList.push(el);

            $scope.$apply();
        });
        
        socket.on('connect', function () {
            console.log("authenticated");
            $scope.txtUsername = user;
        }).on("disconnect", function () {
            console.log("disconnected");
        });

        $scope.selectRoom = function(room){
            if(room)
                socket.emit("selectRoom", {room: room});
            else if($scope.newRoomName)
                socket.emit("selectRoom", {room: $scope.newRoomName});

            if(room || $scope.newRoomName){
                $scope.showRooms = false;
                $scope.showControls = true;
            }
        };
        
        socket.on("send", function (data) {
            //console.log(data);
            setHtml(data, "messageOther");
        });
        
        socket.on("busy", function () {
            $(".submit").prop("disabled", true);
        });
        
        socket.on("free", function () {
            $(".submit").prop("disabled", false);
        });
    };
    
    var setHtml = function (data, userClassName) {
        var user;
        
        if (userClassName === "messageSelf") user = "You"; else user = data.username;
        
        var morse = MorseTeken.textToMorse(data.message);
        
        var html = $scope.messageBoard;
        html += "<div class='message'>";
        html += "<span class='" + userClassName + "'>" + user + ": </span>";
        html += "<span>" + data.message + "</span>";
        html += "<p> Morse: " + morse + "</p>";
        html += "</div>";
        
        //console.log(html)
        
        $scope.messageBoard = $sce.trustAsHtml(html);
        $scope.$apply();
        
        var el = document.getElementById("messageBoard");
        el.scrollTop = el.scrollHeight;
        
        socket.emit("busy");
        
        $scope.playMorse(morse);
    };
    
    var init = function () {
        var obj = sessionStorage.getItem("loginObject");
        if (obj) {
            obj = JSON.parse(obj);
            var now = new Date();
            now = now.toGMTString();
            if (Date.parse(now) < Date.parse(obj.expires)) {
                token = obj.token;
                $scope.txtUsername = obj.username;
                
                connect_socket(token, obj.username);
                $(".chatDiv").css("display", "none");
                $scope.showRooms = true;
            }
        }
    };
    
    var Error = function (message) {
        console.log(message);
        $scope.errorMessage = message;
        $scope.$apply();
    };
    
    init();
};

ChatController.$inject = ["$scope", "$sce"];

