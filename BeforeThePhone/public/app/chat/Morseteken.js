﻿function MorseTeken(text, morse) {
    this.text = text;
    this.morse = morse;
}

MorseTeken.prototype = {
    get Morse() { return this.morse; },
    set Morse(v) { this.morse = v; },
    
    get Text() { return this.text; },
    set Text(v) { this.text = v; },
};

MorseTeken.MorseAlfabet = [
    new MorseTeken(" ", "/"),
    new MorseTeken("A", ".-"),
    new MorseTeken("B", "-..."),
    new MorseTeken("C", "-.-."),
    new MorseTeken("D", "-.."),
    new MorseTeken("E", "."),
    new MorseTeken("F", "..-."),
    new MorseTeken("G", "--."),
    new MorseTeken("H", "...."),
    new MorseTeken("I", ".."),
    new MorseTeken("J", ".---"),
    new MorseTeken("K", "-.-"),
    new MorseTeken("L", ".-.."),
    new MorseTeken("M", "--"),
    new MorseTeken("N", "-."),
    new MorseTeken("O", "---"),
    new MorseTeken("P", ".--."),
    new MorseTeken("Q", "--.-"),
    new MorseTeken("R", ".-."),
    new MorseTeken("S", "..."),
    new MorseTeken("T", "-"),
    new MorseTeken("U", "..-"),
    new MorseTeken("V", "...-"),
    new MorseTeken("W", ".--"),
    new MorseTeken("X", "-..-"),
    new MorseTeken("Y", "-.--"),
    new MorseTeken("Z", "--.."),
    new MorseTeken("0", "-----"),
    new MorseTeken("1", ".----"),
    new MorseTeken("2", "..---"),
    new MorseTeken("3", "...--"),
    new MorseTeken("4", "....-"),
    new MorseTeken("5", "....."),
    new MorseTeken("6", "-...."),
    new MorseTeken("7", "--..."),
    new MorseTeken("8", "---.."),
    new MorseTeken("9", "----."),
    new MorseTeken(".", ".-.-.-"),
    new MorseTeken(",", "--..--"),
    new MorseTeken("?", "..--.."),
    new MorseTeken("!", "-.-.--"),
    new MorseTeken("-", "-....-"),
    new MorseTeken("/", "-..-."),
    new MorseTeken(":", "---..."),
    new MorseTeken("'", ".----."),
    new MorseTeken(")", "-.--.-"),
    new MorseTeken(";", "-.-.-"),
    new MorseTeken("(", "-.--."),
    new MorseTeken("=", "-...-"),
    new MorseTeken("@", ".--.-."),
    new MorseTeken("&", ".-..."),
    new MorseTeken('"', ".-..-."),
];

MorseTeken.textToMorse = function (text) {
    var arr = text.split("");
    var morse = "";

    angular.forEach(arr, function (value) {
        var str = MorseTeken.getValueOfAlfabet(value, 0);
        morse += str;
        morse += " ";
    });

    return morse;
};

MorseTeken.morseToText = function (morse) {
    morse = morse.trim();
    var arr = morse.split(" ");
    var text = "";

    angular.forEach(arr, function (value) {
        var str = MorseTeken.getValueOfAlfabet(value, 1);
        text += str;
        Text += " ";
    });

    return text;
};

MorseTeken.getValueOfAlfabet = function (string, type) {
    var val = "";
    angular.forEach(MorseTeken.MorseAlfabet, function (value) {
        switch (type) {
            case 0:
                if (value.Text == string.toUpperCase())
                    val = value.Morse;
                break;
            case 1:
                if (value.Morse == string)
                    val = value.Text;
                break;
            default:
                return null;
        }
    });

    return val;
};