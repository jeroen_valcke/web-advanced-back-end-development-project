﻿var NewsController = function ($scope) {
    $scope.papers = getPapers();
    $scope.newsDetail = null;
    $scope.showOverview = true;
    $scope.showDetail =false;
    
    function getPapers() {
        return [
            new Paper("Los Angeles Times", "Los_Angeles_Times.jpg"),
            new Paper("New York Times", "New_York_Times.png"),
            new Paper("The Washington Times", "Washington.jpg"),
            new Paper("Daily Express", "Daily_Express.jpg"),
            new Paper("The Oregonian", "The_Oregonian.jpg"),
            new Paper("Star Bulletin", "Star_Bulletin.jpg"),
            new Paper("The Russiaville Observer", "The_Russiaville_Observer.jpg"),
            new Paper("The San Fransisco Call", "The_San_Fransisco_Call.jpg")
        ];
    }
    
    $scope.detail = function (titel) {
        angular.forEach($scope.papers, function (val) {
            if (val.Titel == titel) {
                $scope.newsDetail = val;
                $scope.showDetail = true;
                $scope.showOverview = false;
                
                $("#newsDetail_img").attr("data-zoom-image", "/img/news/big/" + $scope.newsDetail.Img);
                $("#newsDetail_img").prop("src", "/img/news/small/" + $scope.newsDetail.Img);

                elevateZoom();      
            }
        });
    };

    $scope.backNews = function(){
        if($scope.showDetail === true){
            $scope.showOverview = true;
            $scope.showDetail = false;
        }else{
            $scope.home();
        }
    };
    
    function magnifyZoom(){
        $("#newsDetail_img").magnify();
    }
    
    function elevateZoom(){
        console.log("zoom");
        $("#newsDetail_img").elevateZoom({
            zoomType: "lens",
            lensShape: "round",
            lenszoom:true,
            lensSize: 230,
            scrollZoom: true,
            easing: true, 
            constrainType: "height", 
            constrainSize: 0,
            containLensZoom: true
        });
        console.log("zoom initiated");
    }
    
    function gzoom() {
        $("#newsDetail_img").gzoom();
    }
    
    function zoom() {
        wheelzoom(document.getElementById('newsDetail_img'));
    }
    
    function initiatePanZoom() {
        var $panzoom = $('#newsDetails_img_container').panzoom();
        $panzoom.parent().on('mousewheel.newsDetails_container', function (e) {
            e.preventDefault();
            var delta = e.delta || e.originalEvent.wheelDelta;
            var zoomOut = delta ? delta < 0 : e.originalEvent.deltaY > 0;
            $panzoom.panzoom('zoom', zoomOut, {
                increment: 0.1,
                minScale: 1,
                animate: false,
                focal: e
            });
        });
        
        $panzoom.panzoom("resetDimensions");
    }
};

NewsController.$inject = ["$scope"];