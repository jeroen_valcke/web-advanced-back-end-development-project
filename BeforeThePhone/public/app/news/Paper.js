﻿function Paper(titel, img, tumb) {
    this.titel = titel;
    this.img = img;
    this.tumb = tumb;
}

Paper.prototype = {
    set Titel(v) { this.titel = v; },
    get Titel() { return this.titel; },
    
    set Img(v) { this.img = v; },
    get Img() { return this.img; },
    
    set Tumb(v) { this.tumb = v; },
    get Tumb() { return this.tumb; },
};
