﻿var TvController = function ($scope) {
    var videos = ["L4tDqjpecqM", "tEDMDpvUyv0", "u0IU8uQniX8", "bcFx4gQRm6o", "tXTmYksu3OQ", "-cyKRNCWZN8"];
    //var videos = ["LOggD17l5G4", "tEDMDpvUyv0", "u0IU8uQniX8", "bcFx4gQRm6o", "tXTmYksu3OQ", "-cyKRNCWZN8"];  
    var players = [];
    var ruis;
    var rotateParams;
    var dragging = false;
    var target;
    var playingVideoIndex;
    var verzet;
    
    
    var init = function () {
        ruis = $("#ruis");
        target = $("#tv-control>img");
        
        initJQueryKnob();       
        
        if ($("#youtube_api").length === 0) {
            console.log("add script");
            var tag = document.createElement('script');
            tag.setAttribute("id", "youtube_api");
            tag.src = "https://www.youtube.com/player_api";
            var firstScriptTag = document.getElementsByTagName('script')[0];
            firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
        } else {
            onYouTubePlayerAPIReady();
        }
        
        // Replace the 'ytplayer' element with an <iframe> and
        // YouTube player after the API code downloads.
    };
    
    var initJQueryKnob = function () {
        $(".dial").knob({
            'min': 0,
            'max': 360,
            'width': 25,
            'height': 25,
            'tickness': 10,
            'change': function (v) { updateKnob(v); updateTv(v); }
        });
    };
    
    
    var updateKnob = function (v) {
        console.log(v);
        target.css('-moz-transform', 'rotate(' + v + 'deg)');
        target.css('-moz-transform-origin', '50% 50%');
        target.css('-webkit-transform', 'rotate(' + v + 'deg)');
        target.css('-webkit-transform-origin', '50% 50%');
        target.css('-o-transform', 'rotate(' + v + 'deg)');
        target.css('-o-transform-origin', '50% 50%');
        target.css('-ms-transform', 'rotate(' + v + 'deg)');
        target.css('-ms-transform-origin', '50% 50%');

    };
    
    var updateTv = function (v) {
        var treshold = (360 / 6);
        var tresholdDiference = v % treshold;
        var miniTreshold = treshold / 3;
        
        var val = 0;
        var index;
        var oldPl;
        var pl;
        var oldDiv;
        var div;
        
        if (!((tresholdDiference < miniTreshold) || (tresholdDiference > (miniTreshold * 2)))) {
            var temp = tresholdDiference - miniTreshold;
            val = temp / miniTreshold;
            if (val > 1 && verzet === false) {
                index = playingVideoIndex + 1;
                if (index >= players.length) index = 0;
                oldPl = players[playingVideoIndex];
                pl = players[index];
                console.log(index);
                oldDiv = oldPl.d.id;
                div = pl.d.id;
                console.log(div);
                
                muteAllAndHideDivs();
                $("#" + div).css({ "opacity": 1 });
                pl.unMute();
                
                
                val = 1 - (val - 1);
                playingVideoIndex = index;
                verzet = true;
            } else if (verzet === false) {
                index = playingVideoIndex - 1;
                if (index < 0) index = players.length - 1;
                oldPl = players[playingVideoIndex];
                pl = players[index];
                console.log(index);
                oldDiv = oldPl.d.id;
                div = pl.d.id;
                console.log(div);
                
                muteAllAndHideDivs();
                $("#" + div).css({ "opacity": 1 });
                pl.unMute();
                
                
                playingVideoIndex = index;
                verzet = true;
            }
        } else {
            verzet = false;
        }
        
        ruis.css({ 'opacity': val });
    };
    
    var muteAllAndHideDivs = function () {
        for (var i = 0; i < players.length; i++) {
            players[i].mute();
            $(".zenders").css({ 'opacity': 0 });
            console.log("mute");
        }
    };

    var player;
    window.onYouTubePlayerAPIReady = function () {
        for (var i = 0; i <= videos.length; i++) {
            initPlayer(i);
        }        
    };
    var initPlayer = function (i) {
        var zender = "zender" + (i + 1);
        player = new YT.Player(zender, {
            height: '135',
            width: '165',
            videoId: videos[i],
            playerVars: {
                'autoplay': 1,
                'controls': 0, 
                'rel' : 0,
                'loop': 1,
                'playlist': videos[i]
            },
            events: {
                'onReady': onPlayerReady
            }
            
        });
    };
    
    window.onPlayerReady = function (event) {
        players.push(event.target);
        event.target.playVideo();
        if (event.target.d.id != "zender1") {
            event.target.mute();
            playingVideoIndex = players.length - 1;
        }
        animateRuisOpacity(0);
    };
    
    window.animateRuisOpacity = function (opacity) {
        $("#ruis").animate({ "opacity": opacity }, 1000);
    };
   

    init();
};

TvController.$inject = ["$scope"];