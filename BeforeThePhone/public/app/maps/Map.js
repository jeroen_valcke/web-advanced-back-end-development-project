﻿function Map(x, y) {
    this.x = x;
    this.y = y;
}

Map.prototype = {
    //getters & setters
    set X(v) { this.x = v; },
    get X() { return this.x; },
    
    set Y(v) { this.y = y; },
    get Y() { return this.y; },
    
    get Options() {
        return {
            zoom: 12,
            center: new google.maps.LatLng(this.X, this.Y),
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            styles: this.StyleArray,
            streetViewControl: false,
            mapTypeControl: false
        };
    },
    
    get StyleArray() {
        return [
            {
                "featureType": "landscape.natural",
                "stylers": [
                    {
                        "saturation": -100
                    },
                    {
                        "lightness": 100
                    }
                ]
            },
            {
                "featureType": "water",
                "stylers": [
                    {
                        "saturation": -100
                    },
                    {
                        "lightness": -86
                    }
                ]
            },
            {
                "elementType": "labels.text.stroke",
                "stylers": [
                    {
                        "saturation": -100
                    },
                    {
                        "lightness": 100
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "geometry.stroke",
                "stylers": [
                    {
                        "saturation": -100
                    },
                    {
                        "lightness": -75
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "saturation": -100
                    },
                    {
                        "lightness": 97
                    }
                ]
            },
            {
                "featureType": "poi.park",
                "stylers": [
                    {
                        "saturation": -100
                    },
                    {
                        "lightness": -100
                    }
                ]
            },
            {
                "featureType": "poi.park",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "saturation": -100
                    },
                    {
                        "lightness": 100
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "labels",
                "stylers": [
                    {
                        "visibility": "on"
                    }
                ]
            },    
            {
                "featureType": "administrative",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "saturation": -100
                    },
                    {
                        "lightness": 100
                    }
                ]
            },
            {
                "featureType": "administrative",
                "elementType": "labels.text.stroke",
                "stylers": [
                    {
                        "saturation": -100
                    },
                    {
                        "lightness": -100
                    }
                ]
            },
            {
                "featureType": "poi",
                "stylers": [
                    {
                        "saturation": -100
                    },
                    {
                        "lightness": 91
                    }
                ]
            },
            {
                "featureType": "poi",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "saturation": -100
                    },
                    {
                        "lightness": -100
                    }
                ]
            },
            {
                "featureType": "transit.station",
                "stylers": [
                    {
                        "saturation": -100
                    },
                    {
                        "lightness": -22
                    }
                ]
            },
            {
                "featureType": "landscape.man_made",
                "elementType": "geometry.stroke",
                "stylers": [
                    {
                        "hue": "#ff004c"
                    },
                    {
                        "saturation": -100
                    },
                    {
                        "lightness": 44
                    }
                ]
            },
            {
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "saturation": 1
                    },
                    {
                        "lightness": -100
                    }
                ]
            },
            {
                "elementType": "labels.text.stroke",
                "stylers": [
                    {
                        "saturation": -100
                    },
                    {
                        "lightness": 100
                    }
                ]
            },
            {
                "featureType": "administrative.locality",
                "elementType": "labels",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "water",
                "elementType": "labels",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "administrative.locality",
                "elementType": "labels",
                "stylers": [
                    {
                        "visibility": "on"
                    }
                ]
            },
            {
                "featureType": "water",
                "elementType": "labels",
                "stylers": [
                    {
                        "visibility": "on"
                    }
                ]
            }
        ];

    },

    // functions
   
};