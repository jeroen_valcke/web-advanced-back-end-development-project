﻿var MapController = function ($scope) {
    
    var init = function () {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(initMap);
        } else {
            initMap();
        }
    };
    
    window.initialize = function () {
        init();
    };
    
    function initMap(position) {
        var m;
        if (position)
            m = new Map(position.coords.latitude, position.coords.longitude);
        else
            m = new Map(0, 0);
        
        var myOptions = m.Options;
        
        var elMap = $("#maps");
        
        var map = new google.maps.Map(elMap[0], myOptions);
        
        var drawingManager = new google.maps.drawing.DrawingManager({
            drawingMode: google.maps.drawing.OverlayType.POLYLINE,
            drawingControl: true,
            drawingControlOptions: {
                position: google.maps.ControlPosition.TOP_CENTER,
                drawingModes: [
                    google.maps.drawing.OverlayType.POLYLINE
                ]
            },
            polylineOptions: {
                strokeColor: '#0008FF',
            }
        });
        
        $("#maps>img").remove();
        
        drawingManager.setMap(map);
    }
};

MapController.$inject = ["$scope"];