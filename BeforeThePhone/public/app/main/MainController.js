﻿var MainController = function ($scope) {
    var smartphone = $("#smartphone");
    $scope.home = "/app/home/home.html";
    $scope.detail = "/app/details/home.html";
    $scope.changeDetail = function (app) {
        $scope.detail = "/app/details/" + app + ".html";
    };

    $scope.changeTab = function (div) {
        $(".detail_tab").removeClass("detail_tab_selected");
        $(".tab_" + div).addClass("detail_tab_selected");
        
        $(".detail_tab_content").css("display", "none");
        $("." + div).css("display", "block");
    };

    $scope.mobileInfo = function () {
        var el = $("#context_info");
        console.log(el.css("left"));
        if (el.css("left") != "0px") {
            el.animate({ "left": 0 });
        } else {
            
            el.animate({ "left": "-800px" });
        }
    };
};

MainController.$inject = ["$scope"];