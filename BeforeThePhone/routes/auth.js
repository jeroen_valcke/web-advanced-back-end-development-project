var express = require('express');
var router = express.Router();
var passport = require('passport');
var passportConfig = require('../config/passport.js')(passport);
var jwt = require("jsonwebtoken");
var jwtSecret = require('../config/jwtSecret');

router.post('/signup', passport.authenticate('local-signup', {
    successRedirect: '/auth/signupOk',
    failureRedirect: '/auth/signupFailed',
    failureFlash: true
}));

router.get('/signupOk', isLoggedIn, function (req, res) {
    var token = jwt.sign(req.user, jwtSecret, { expiresInMinutes: 60 * 5 });

    var now = new Date();
    var time = now.getTime();
    var expireTime = time + 1000 * 3600 * 5;
    now.setTime(expireTime);

    res.send({ token: token, username: req.user.local.username, expires: now, status: 200 });
});

router.get('/signupFailed', function (req, res) {
    res.send(400);
});

router.get('/loginOk', isLoggedIn, function (req, res) {
    var token = jwt.sign(req.user, jwtSecret, { expiresInMinutes: 60 * 5 });

    var now = new Date();
    var time = now.getTime();
    var expireTime = time + 1000 * 3600 * 5;
    now.setTime(expireTime);

    res.send({ token: token, username: req.user.local.username, expires: now, status: 200 });
});

router.get('/loginFailed', function (req, res) {
    res.send(400);
});

router.post('/login', passport.authenticate('local-login', {
    successRedirect: '/auth/loginOk',
    failureRedirect: '/auth/loginFailed',
    failureFlash: true
}));

router.post('/logout', function (req, res) {
    req.logout();
});

function isLoggedIn(req, res, next) {
    if (req.isAuthenticated())
        return next();
}

module.exports = router;
