var express = require('express');
var app = express();
var server = app.listen((process.env.PORT || 1337), function () {
    console.log("server started on port " + process.env.PORT);
});
var io = require("socket.io").listen(server);
var logger = require('morgan');
var bodyParser = require('body-parser');
var cookieParser = require("cookie-parser");
var mongoose = require("mongoose");
var session = require("express-session");
var passport = require("passport");
var flash = require("connect-flash");
var path = require("path");
var configDB = require("./config/database.js");

var auth = require("./routes/auth");

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());
app.use(require('stylus').middleware(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'public')));
app.use(session({secret: 'wazisthis?IdontNow' }));
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());

mongoose.connect(configDB.url);

app.get("/", function (req, res) {
    res.render("index");
});

app.use('/auth', auth);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function (err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});

var talk = require("./socket/talk.js")(io);

module.exports = app;